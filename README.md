# Sarcomere segmentation

## Requirements

The software was tested on `Python 3.7.11` with the following packages:

    - skimage 0.16.2
    - tensorflow 1.14.0
    - keras 2.2.4
    - sklearn 1.0.2
    - csv 1.0
    - numpy 1.20.3
    - matplotlib 3.5.1
    - PIL 8.4.0
    - h5py 2.10.0

## Training

The training process requires a coma separated value (*.csv) file where each line contains data for a single localization. The first 350 columns are the distances of the nearest neighbours to the given localization measured in nanometers. Rows 351-380 are the angular distribution of the 350 nearest neighbours. To do this, assign a direction (angle value) to the neighboring localizations and then generate an angular distribution from these (a histogram with 30 bins) which should be smoothed with a kernel size of 3 to make it less noisy. The last column is the label indicating whether the given localization is object (1) or noise (0).

Workflow:

    1. Open and run the training_sarcomere_segmentation.py script.
    2. In a pop-up window, navigate to the folder where the training data set is.
    3. In a pop-up window, navigate to the folder where the model should be saved.
    4. Wait until the 'Saved to disk' message appears on the terminal.

## Segmentation

The localization data file to be analysed must be in csv format and the columns must be comprised of the following headers and data:

    - frame_idx -> camera frame index of the localization
    - x_coord -> x coordinate of the localization measured in nanometers
    - y_coord -> y coordinate of the localization measured in nanometers
    - x_std  -> localization precision (calculated as standard deviation) along the x axis measured in nanometers
    - y_std -> localization precision (calculated as standard deviation) along the y axis measured in nanometers
    - I -> height of the fitted Gaussian in camera counts
    - sig_x -> width (in standard deviation) of the fitted Gaussian along the x axis measured in camera pixel lengths
    - sig_y -> width (in standard deviation) sigma of the fitted Gaussian along the y axis measured in camera pixel lengths
    - Sum_signal -> calculated total intensity in camera counts

Only the `x` and `y` coordinates are crucial for the segmentation, but further analysis in the [IFM analyser](http://titan.physx.u-szeged.hu/~adoptim/?page_id=1246) may require the other data.

Workflow:

    1. Open and run sarcomere_segmentation.py.
    2. In a pop-up window, navigate to the folder where the measurement `csv` files are.
    3. In the selected folder, the program creates a folder named 'results'. ATTENTION: IF THERE IS ALREADY A FOLDER WITH THIS NAME, IT WILL OVERWRITE IT!

The program will then automatically run for all localization files. From the localizations, a `png` file is generated and saved in the results folder, where the ROIs surrounding the identified objects are marked with a red square. The program runs the classification for each ROI and saves a `csv` file with the localizations assigned to the object and a summary plot of the localizations accepted and filtered out. Meanwhile, the amount of time taken for object detection and classification is always displayed.

