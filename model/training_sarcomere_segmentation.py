import numpy as np
import tensorflow as tf
import tensorflow.keras.optimizers
from sklearn.preprocessing import minmax_scale
from sklearn.model_selection import train_test_split

from tkinter import filedialog
from tkinter import Tk

import csv

from numpy import genfromtxt

epoch_num=20
ditance_feature_length=350
angle_feature_length=30
max_feature_length=ditance_feature_length+angle_feature_length


# load the data
root = Tk()
root.withdraw()
files = [('CSV Files', '*.csv'), 
            ('All Files', '*.*')]
path2trnData = filedialog.askopenfilename(title='Open a training data set', filetypes = files)

arr = genfromtxt(path2trnData, delimiter=',')
# extracting features
X = arr[:, :-1] 

nn_distances=X[:,np.arange(ditance_feature_length)];
angle_distro=X[:, np.arange(ditance_feature_length, max_feature_length)];

root = Tk()
root.withdraw()
files = [('HDF5 Files', '*.h5'), 
            ('All Files', '*.*')]
modelFileName = filedialog.asksaveasfile(title='Where the model should be saved to...', filetypes = files, defaultextension = files)


# normalization to 0-1 - per column/feature
for point in range(nn_distances.shape[0]):
    nn_distances[point,:]=minmax_scale(nn_distances[point,:])
    
for point in range(angle_distro.shape[0]):
    angle_distro[point,:]=minmax_scale(angle_distro[point,:])
    

X = np.concatenate((nn_distances,angle_distro),axis=1)

# labels
Y = arr[:, -1]


# validation and test set creation
x_train, x_val, y_train, y_val = train_test_split(X, Y, test_size=0.2)
x_val, x_test, y_val, y_test = train_test_split(x_val, y_val, test_size=0.5)



def build_model():
    classifier_model = tf.keras.Sequential()
    classifier_model.add(tf.keras.layers.Dense(32, activation='relu', input_shape=x_train.shape[1:]))
    classifier_model.add(tf.keras.layers.Flatten())
    classifier_model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
    return classifier_model


model = build_model()
model.compile(loss='binary_crossentropy', optimizer=tf.keras.optimizers.Adam(), metrics=['accuracy'])
model.summary()

# train the model
# gave some value for the model training, it might be worth to try different values too
history = model.fit(x_train, y_train, batch_size=32, epochs=epoch_num, validation_data=(x_val, y_val))

loss_training, accuracy_training = model.evaluate(x_test, y_test, verbose=1)
print('\tAccuracy:\t%.2f%%\n\tLoss:\t\t%.2f' % (accuracy_training * 100, loss_training))

# save the trained model
model.save(modelFileName)
print("Saved model to disk")
