#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  2 13:38:24 2022

@author: vd
"""

# info: required python and package versions
'''
Python 3.7.11
IPython 7.31.1

skimage 0.16.2
tensorflow 1.14.0
keras 2.2.4-tf
sklearn 1.0.2
csv 1.0
numpy 1.20.3
matplotlib 3.5.1
PIL 8.4.0
h5py 2.10.0
conda install -c conda-forge faiss-cpu # not necessary

'''
# import packages
from tkinter import filedialog
from tkinter import Tk
import os
import time

import csv
import math
import numpy as np
from scipy.spatial import ConvexHull as convhull

import skimage
from PIL import Image,ImageEnhance
from matplotlib import pyplot
from matplotlib.patches import Rectangle

from mrcnn.model import mold_image
from mrcnn.config import Config
from mrcnn.model import MaskRCNN

import tensorflow as tf
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import minmax_scale


########################## functions ##########################

# read in measurement csv file
def measDataRead(path2m):
    file = open(path2m)
    csvreader = csv.reader(file)
    header = next(csvreader)
    #print(header)
    rows = []
    for row in csvreader:
        rows.append(row)
    #print(rows)
    file.close()
    meas_data=np.asarray(rows).astype(float)

    # get the relevant data: x_coord,y_coord,x_std,y_std,I,sig_x,sig_y 
    frame_idx=header.index("frame_idx")
    x_idx=header.index("x_coord")
    y_idx=header.index("y_coord")
    std_x_idx=header.index("x_std")
    std_y_idx=header.index("y_std")
    I_idx=header.index("I")
    sig_x_idx=header.index("sig_x")
    sig_y_idx=header.index("sig_y")
    sum_signal_idx=header.index("Sum_signal")
    
    frame=meas_data[:,frame_idx]
    x_coord=meas_data[:,x_idx]
    y_coord=meas_data[:,y_idx]
    x_std=meas_data[:,std_x_idx]
    y_std=meas_data[:,std_y_idx]
    I=meas_data[:,I_idx]
    sig_x=meas_data[:,sig_x_idx]
    sig_y=meas_data[:,sig_y_idx]
    sum_signal=meas_data[:,sum_signal_idx]
    
    N_loc=x_coord.shape[0]
    
    frame=np.expand_dims(frame, axis=1)
    x_coord=np.expand_dims(x_coord, axis=1)
    y_coord=np.expand_dims(y_coord, axis=1)
    x_std=np.expand_dims(x_std, axis=1)
    y_std=np.expand_dims(y_std, axis=1)
    I=np.expand_dims(I, axis=1)
    sig_x=np.expand_dims(sig_x, axis=1)
    sig_y=np.expand_dims(sig_y, axis=1)
    sum_signal=np.expand_dims(sum_signal, axis=1)
    meas_params=np.concatenate((frame,x_coord,y_coord,x_std,y_std,I,sig_x,sig_y,sum_signal),axis=1)
    
    # shuffle rows in order to avoid high-frequency blinking created structures - trn data resemblance
    np.take(meas_params,np.random.permutation(meas_params.shape[0]),axis=0,out=meas_params)
    
    return meas_params,N_loc

# generate pixelated image for Mask-RCNN object detection
def imgGen(selected_x,selected_y,img_x_edges,img_y_edges,factor):
    img_hist=pyplot.hist2d(selected_x,selected_y,bins=[img_x_edges,img_y_edges])
    img_raw=img_hist[0]
    max_pix_value=np.amax(img_raw)
    norm_value=max_pix_value
    scale_factor=255/norm_value
    img_scaled=np.around(img_raw*scale_factor)
    img=img_scaled.astype(np.uint8)
    img_rgb=skimage.color.gray2rgb(img)
    
    # enhance contrast test
    im = Image.fromarray(img_rgb)
    enhancer=ImageEnhance.Contrast(im)
    im=enhancer.enhance(factor)
    img_rgb = np.array(im)
    return img_rgb


# define the prediction configuration
class PredictionConfig(Config):
	# define the name of the configuration
	NAME = "trn_img_gen_cfg"
	# number of classes (background + kangaroo)
	NUM_CLASSES = 1 + 1
	# simplify GPU config
	GPU_COUNT = 1
	IMAGES_PER_GPU = 1
    

def interval_overlap(xmin1,xmax1,xmin2,xmax2):
    if xmin2<=xmax1 and xmax2>=xmin1:
        overlap=True
    else:
        overlap=False
    return overlap

# object detection - find boxes
def findObjectROI(img_rgb,cfg,obj_det_model):
    # convert pixel values (e.g. center)
    scaled_image = mold_image(img_rgb, cfg)
    # convert image into one sample
    sample = np.expand_dims(scaled_image, 0)
    # make prediction
    yhat = obj_det_model.detect(sample, verbose=0)[0]
    # get boxes
    try:
        box=yhat['rois']
    except:
        box=yhat[0]['rois']

    # get ROIs from boxes  
    merged_box=np.full([box.shape[0],box.shape[1]],None)
    merg_idx=0
    for box_i in box:
        merged_box[merg_idx] = box_i
        for box_j in box:
            ymin1, xmin1, ymax1, xmax1 = merged_box[merg_idx]
            ymin2, xmin2, ymax2, xmax2 = box_j
            if interval_overlap(xmin1,xmax1,xmin2,xmax2) and interval_overlap(ymin1,ymax1,ymin2,ymax2):
                xmin=min([xmin1,xmin2])
                ymin=min([ymin1,ymin2])
                xmax=max([xmax1,xmax2])
                ymax=max([ymax1,ymax2])
                merged_box[merg_idx]=ymin, xmin, ymax, xmax
        merg_idx=merg_idx+1
    rois = np.unique(merged_box.astype(int),axis=0)
    
    merged_box2=np.full([rois.shape[0],rois.shape[1]],None)
    merg_idx=0
    for box_i in rois:
        merged_box2[merg_idx] = box_i
        for box_j in rois:
            ymin1, xmin1, ymax1, xmax1 = merged_box2[merg_idx]
            ymin2, xmin2, ymax2, xmax2 = box_j
            if interval_overlap(xmin1,xmax1,xmin2,xmax2) and interval_overlap(ymin1,ymax1,ymin2,ymax2):
                xmin=min([xmin1,xmin2])
                ymin=min([ymin1,ymin2])
                xmax=max([xmax1,xmax2])
                ymax=max([ymax1,ymax2])
                merged_box2[merg_idx]=ymin, xmin, ymax, xmax
        merg_idx=merg_idx+1
    rois2 = np.unique(merged_box2.astype(int),axis=0)
    
    return rois2

# plot object detection result
def plotObjDetect(img_rgb,act_rois,img_pix_size,path2saveImg):
    rois=act_rois/img_pix_size
    pyplot.figure()
    pyplot.imshow(img_rgb)
    pyplot.title('Predicted ROIs')
    ax = pyplot.gca()
    # plot each box
    for box_i in rois:
    	# get coordinates
    	y1, x1, y2, x2 = box_i
    	# calculate width and height of the box
    	width, height = x2 - x1, y2 - y1
    	# create the shape
    	rect = Rectangle((x1, y1), width, height, fill=False, color='red', linewidth=0.3)
    	# draw the box
    	ax.add_patch(rect)
    pyplot.savefig(path2saveImg, dpi=300)
    pyplot.close()
    
# split localizations into ROI to sets
def splitListCalc(selected_rois,roi_idx,plus_search_dist,loc_x,loc_y,meas_params,roi_corr,object_loc_num):
    xmin_analyse=selected_rois[roi_idx][0]-plus_search_dist
    xmax_analyse=selected_rois[roi_idx][2]+plus_search_dist
    ymin_analyse=selected_rois[roi_idx][1]-plus_search_dist
    ymax_analyse=selected_rois[roi_idx][3]+plus_search_dist
    
    # localization parameters in actual ROI
    in_x=np.logical_and(loc_x>=xmin_analyse, loc_x<=xmax_analyse)
    in_y=np.logical_and(loc_y>=ymin_analyse, loc_y<=ymax_analyse)
    selected_meas_params=meas_params[np.logical_and(in_x,in_y),:]
    
    # estimated localiztaions in the actual object
    xmin_obj=selected_rois[roi_idx][0]-roi_corr
    xmax_obj=selected_rois[roi_idx][2]+roi_corr
    ymin_obj=selected_rois[roi_idx][1]-roi_corr
    ymax_obj=selected_rois[roi_idx][3]+roi_corr
    
    in_obj_x=np.logical_and(loc_x>=xmin_obj, loc_x<=xmax_obj)
    in_obj_y=np.logical_and(loc_y>=ymin_obj, loc_y<=ymax_obj)
    act_est_obj_x=meas_params[np.logical_and(in_obj_x,in_obj_y),0]
    
    # split coordinates into sets with object_loc_num coordinates
    double_line_loc_num=len(act_est_obj_x)
    roi_loc_num=selected_meas_params.shape[0]
    set_num=math.floor(double_line_loc_num/object_loc_num)
    if set_num<1:
        set_num=1
    locs_per_set=roi_loc_num/set_num
    split_list=np.arange(0, roi_loc_num+locs_per_set, locs_per_set).tolist()
    split_list = [round(num) for num in split_list]
    if split_list[-1]!=roi_loc_num:
        split_list[-1]=roi_loc_num
        
    v = [split_list[i+1]-split_list[i] for i in range(len(split_list)-1)]
    min_set_num=min(v)
    
    return roi_loc_num,selected_meas_params,split_list,min_set_num

# create feature vector for classification
def featureVecCalc(selected_x_set,selected_y_set,histogram_bin_num):
    # get featrure vectors
    # get distances
    coord_set=np.concatenate((np.expand_dims(selected_x_set, axis=1),np.expand_dims(selected_y_set, axis=1)), axis=1)
    
    # method 1
    nbrs = NearestNeighbors(n_neighbors=analysed_neighbours+1, algorithm='auto').fit(coord_set)
    distances, indices = nbrs.kneighbors(coord_set)
    # cut off irrelevant part
    distances=distances[:,1:]
    indices=indices[:,1:]

    # # method2
    # d = 2                           # dimension
    # xb=coord_set.astype('float32')
    # xq=coord_set.astype('float32')
       
    # index = faiss.IndexFlatL2(d)   # build the index
    # index.add(xb)                  # add vectors to the index
    # k = analysed_neighbours+1                          # we want to see 4 nearest neighbors
    # distances, indices = index.search(xq, k)     # actual search
    # # cut off irrelevant part
    # distances=distances[:,1:]
    # distances=np.sqrt(distances)
    # indices=indices[:,1:]
    
    # get directions
    histogram_bin_start=-90*(math.pi/180)
    histogram_bin_step=math.pi/histogram_bin_num
    histogram_bin_end=90*(math.pi/180)+histogram_bin_step
    angle_list=np.arange(histogram_bin_start, histogram_bin_end, histogram_bin_step).tolist()
    angle=np.empty([len(selected_x_set), analysed_neighbours])
    kernel_width=3
    max_pos=round(histogram_bin_num/3) # where to move max peak
    directions=np.full([distances.shape[0],histogram_bin_num],None)
    i=-1
    N=(len(selected_x_set)*analysed_neighbours)
    x_dif=np.full([N,1],None).astype('float32')
    y_dif=np.full([N,1],None).astype('float32')
    for loc_idx in range(len(selected_x_set)):
        for nn_idx in range(analysed_neighbours):
            i=i+1
            y_dif[i,0]=selected_y_set[indices[loc_idx,nn_idx]]-selected_y_set[loc_idx]
            x_dif[i,0]=selected_x_set[indices[loc_idx,nn_idx]]-selected_x_set[loc_idx]
            
    angle=np.arctan2(y_dif,x_dif)
    angle=np.reshape(angle,(len(selected_x_set),analysed_neighbours))
    for loc_idx in range(len(selected_x_set)):
        counts,edges = np.histogram(angle[loc_idx,:], bins=angle_list)
        angle_dist_vals=np.concatenate((counts[-1-math.ceil(kernel_width/2)+1:], counts, counts[:math.ceil(kernel_width/2)]), axis=0)
        smoothed_counts=np.convolve(angle_dist_vals, np.ones(kernel_width)/kernel_width, mode='valid')
        smoothed_counts=smoothed_counts[math.ceil(kernel_width/2)-1:-(math.ceil(kernel_width/2))+1]
        
        max_sav=max(smoothed_counts)
        max_sav_idx=smoothed_counts.tolist().index(max_sav) # max peak place
       
        # shift data to max_pos
        if max_sav_idx<max_pos:
            A=np.concatenate((smoothed_counts[histogram_bin_num-(max_pos-max_sav_idx):histogram_bin_num],smoothed_counts[0:histogram_bin_num-(max_pos-max_sav_idx)]), axis=0)
        elif max_sav_idx>max_pos:
            A=np.concatenate((smoothed_counts[max_sav_idx-max_pos:histogram_bin_num],smoothed_counts[0:(max_sav_idx-max_pos)]), axis=0)
        else:
            A=smoothed_counts

        directions[loc_idx,:]=minmax_scale(A)
        
        norm_distances=np.full((distances.shape),None).astype('float')
    for point in range(distances.shape[0]):
        norm_distances[point,:]=minmax_scale(distances[point,:])
    
    feature_vector = np.concatenate((norm_distances,directions),axis=1).astype('float')
    
    return distances, feature_vector
    

########################## evaluation ##########################

# give needed params
EMCCD_pixel_size=160 # in nm
img_pix_size=20 # in nm
img_start=0
meas_file_id='reviewedSupResParams.full.csv'
object_loc_num=300
analysed_neighbours=350
roi_corr=200
plus_search_dist=1000
plus_search_dist_step=500
histogram_bin_num=30
acepted_loc_radius=1500
min_obj_area=500000

# x coord idx in the meas data - 0: frame_idx, 1: x_coord, 2: y_coord
x_coord_idx=1
y_coord_idx=2

# cluster analysis parameters
epsilon=50
min_num=5

# localization filtering
min_std_xy=0
max_std_xy=20
min_sigma_xy=0.7
max_sigma_xy=1

# create config
cfg = PredictionConfig()
# load trained object detection model
scriptPath = os.path.dirname(os.path.realpath(__file__))
modelDirectory = os.path.join(scriptPath, 'model')
obj_det_model = MaskRCNN(mode='inference', model_dir=modelDirectory, config=cfg)
model_path = os.path.join(modelDirectory, 'mask_rcnn_trn_img_gen_cfg_0005.h5')
obj_det_model.load_weights(model_path, by_name=True)
# load trained localization classification model
loc_class_model = tf.keras.models.load_model(os.path.join(modelDirectory, 'model_tf1_14_obj_300_n_350_bin_30_epoch_10.h5'))
 



"data management"
# ask for folder with measurement files
root = Tk()
root.withdraw()
meas_folder = filedialog.askdirectory()
# create folder for results
directory='results'
path2saveRes = os.path.join(meas_folder, directory)
if not os.path.exists(path2saveRes):
    os.mkdir(path2saveRes)




# read in measurement csv files one by one
onlyfiles = [f for f in os.listdir(meas_folder) if os.path.isfile(os.path.join(meas_folder, f))]
for meas_name in onlyfiles:
    if meas_name[-len(meas_file_id):] == meas_file_id:
        print('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        time_start=time.time()
        meas_id=meas_name[:-len(meas_file_id)-1]
        # get the measurement recon data
        meas_params,N_loc=measDataRead(os.path.join(meas_folder, meas_name))
        meas_params[:,x_coord_idx]=meas_params[:,x_coord_idx]*EMCCD_pixel_size
        meas_params[:,y_coord_idx]=meas_params[:,y_coord_idx]*EMCCD_pixel_size
        loc_x=meas_params[:,x_coord_idx]
        loc_y=meas_params[:,y_coord_idx]





        "object detection"
        # create pixelated images and object detection (find ROIs)
        img_end_x=round(max(loc_x))+img_pix_size
        img_end_y=round(max(loc_y))+img_pix_size
        img_x_edges=np.arange(img_start,img_end_x,img_pix_size)
        img_y_edges=np.arange(img_start,img_end_y,img_pix_size)
        
        roi_search_time_start=time.time()
        max_roi_num=0
        rois_area=0
        for enhance_factor in range(5,50,5):
            img_rgb=imgGen(loc_x,loc_y,img_x_edges,img_y_edges,enhance_factor)
            rois=findObjectROI(img_rgb,cfg,obj_det_model)
            act_rois=rois*img_pix_size
            if act_rois.shape[0]>max_roi_num:
                for roi_i in act_rois:
                    if (roi_i[0] == act_rois[0,0]) and (roi_i[1] == act_rois[0,1]):
                        rois_area2=(roi_i[2]-roi_i[0])*(roi_i[3]-roi_i[1])
                    else:
                        rois_area2=rois_area2+(roi_i[2]-roi_i[0])*(roi_i[3]-roi_i[1])
                if rois_area2>rois_area:
                    selected_rois=act_rois
                    max_roi_num=act_rois.shape[0]
                    rois_area=rois_area2
                        
                # save image with ROIs
                rois_plot_name=meas_id+'_rois.png'
                path2saveImg=os.path.join(path2saveRes, rois_plot_name)
                plotObjDetect(img_rgb,act_rois,img_pix_size,path2saveImg)
                
            elif (act_rois.shape[0]==max_roi_num) and (act_rois.shape[0]>0):
                for roi_i in act_rois:
                    if (roi_i[0] == act_rois[0,0]) and (roi_i[1] == act_rois[0,1]):
                        rois_area2=(roi_i[2]-roi_i[0])*(roi_i[3]-roi_i[1])
                    else:
                        rois_area2=rois_area2+(roi_i[2]-roi_i[0])*(roi_i[3]-roi_i[1])
                if rois_area2>rois_area:
                    selected_rois=act_rois
                    max_roi_num=act_rois.shape[0]
                    rois_area=rois_area2
                # save image with ROIs
                rois_plot_name=meas_id+'_rois.png'
                path2saveImg=os.path.join(path2saveRes, rois_plot_name)
                plotObjDetect(img_rgb,act_rois,img_pix_size,path2saveImg)
                
        roi_search_time_end=time.time()
        if max_roi_num==0:
            print('Finding 0 ROIs in '+meas_id+': '+str(round(roi_search_time_end-roi_search_time_start))+' sec')
        else:
            roi_num=selected_rois.shape[0]
            # calculate selected ROI centers
            roi_center=np.empty([roi_num,2])
            r_i=-1
            for sel_roi_idx in selected_rois:
                r_i=r_i+1
                roi_center[r_i,0]=sel_roi_idx[1]+(sel_roi_idx[3]-sel_roi_idx[1])/2
                roi_center[r_i,1]=sel_roi_idx[0]+(sel_roi_idx[2]-sel_roi_idx[0])/2
            
            print('Finding '+str(roi_num)+' ROIs in '+meas_id+': '+str(round(roi_search_time_end-roi_search_time_start))+' sec')
            print('_______________________________________________')
        
        
        
        if max_roi_num>0:
            # calculate the number of localizations in the ROIs and split them into subsets
            roi_data=[None]*roi_num
            split_data=[None]*roi_num
            all_loc_num=0
            roi_i=-1
            for roi_idx in range(roi_num):
                roi_i=roi_i+1
                min_set_num=0
                plus_search_dist_act=plus_search_dist
                while min_set_num<analysed_neighbours+1:
                    roi_loc_num,selected_meas_data,split_list,min_set_num=splitListCalc(selected_rois,roi_idx,plus_search_dist_act,loc_x,loc_y,meas_params,roi_corr,object_loc_num)
                    plus_search_dist_act=plus_search_dist_act+plus_search_dist_step
                all_loc_num=all_loc_num+roi_loc_num
                roi_data[roi_i]=selected_meas_data
                split_data[roi_i]=split_list
                
            # memory release
            del meas_params
            
            
            
            
            
            
            # Localization classification in each ROI 
            for roi_idx in range(selected_rois.shape[0]):
                roi_coord_idx=0
                
                selected_meas_data=roi_data[roi_idx]
                selected_x=selected_meas_data[:,x_coord_idx]
                selected_y=selected_meas_data[:,y_coord_idx]
                selected_coord_num=selected_x.shape[0]
                
                roi_distances=np.empty([selected_coord_num,analysed_neighbours])
                roi_prediction=np.empty([selected_coord_num])
                
                split_list=split_data[roi_idx]
                split_num=len(split_list)
                local_time_start=time.time()
                # loop through each subset
                for analyse_idx in range(split_num-1):
                    # get actual set coordinates
                    selected_x_set=selected_x[split_list[analyse_idx]:split_list[analyse_idx+1]]
                    selected_y_set=selected_y[split_list[analyse_idx]:split_list[analyse_idx+1]]
                    act_coord_num=len(selected_x_set)
                    # get feaure vector
                    distances, feature_vector=featureVecCalc(selected_x_set,selected_y_set,histogram_bin_num)
                    
                    # classify localizations inside the ROI
                    set_prediction = loc_class_model.predict_proba(feature_vector)
                    #store relevant values
                    roi_distances[roi_coord_idx:roi_coord_idx+act_coord_num]=distances
                    roi_prediction[roi_coord_idx:roi_coord_idx+act_coord_num]=np.squeeze(set_prediction)
                    roi_coord_idx=roi_coord_idx+act_coord_num
                    
                local_time_end = time.time()
                print('Localization classification in ROI '+str(roi_idx)+': '+str(round(local_time_end-local_time_start))+' sec')
                             
                
                
                
                
                
                is_object_coord=roi_prediction>0.5
                # get object localization params
                double_line_data=selected_meas_data[is_object_coord,:]
                
                # filter object localizations
                D=roi_distances[is_object_coord,:10]
                mean_distances=np.mean(D, axis=1)
                is_filt_object_coord=mean_distances<5*np.median(mean_distances)
                double_line_data_filt=double_line_data[is_filt_object_coord,:]
                
                # select object localizations in the vicinity of the actual roi center
                xx_dl=double_line_data_filt[:,x_coord_idx]
                yy_dl=double_line_data_filt[:,y_coord_idx]
                
                xmin_roi=roi_center[roi_idx,1]-acepted_loc_radius
                xmax_roi=roi_center[roi_idx,1]+acepted_loc_radius
                ymin_roi=roi_center[roi_idx,0]-acepted_loc_radius
                ymax_roi=roi_center[roi_idx,0]+acepted_loc_radius
                
                in_x=np.logical_and(xx_dl>=xmin_roi, xx_dl<=xmax_roi)
                in_y=np.logical_and(yy_dl>=ymin_roi, yy_dl<=ymax_roi)
                double_line_data_filt=double_line_data_filt[np.logical_and(in_x,in_y),:]
                
                # cluster analyse the data and select one cluster
                xx=double_line_data_filt[:,x_coord_idx]
                yy=double_line_data_filt[:,y_coord_idx]
                Coord=np.concatenate((np.expand_dims(xx,axis=1),np.expand_dims(yy,axis=1)),axis=1)
                clustering = DBSCAN(eps=epsilon, min_samples=min_num).fit(Coord)
                labels=clustering.labels_
                cluster_elements=-1
                for label_idx in range(max(labels)):
                    clust_coord=Coord[labels==label_idx,:]
                    clust_coord_center_x=np.mean(clust_coord[:,0])
                    clust_coord_center_y=np.mean(clust_coord[:,1])
                    dist_from_cent= ((((clust_coord_center_x - roi_center[roi_idx,1] )**2) + ((clust_coord_center_y - roi_center[roi_idx,0])**2) )**0.5)
                    
                    if dist_from_cent<plus_search_dist and sum(labels==label_idx)>cluster_elements:
                        object_label=label_idx
                        cluster_elements=sum(labels==label_idx)
                            
                if cluster_elements!=-1:
                    double_line_data_filt=double_line_data_filt[labels==object_label,:]
                    
                # filter out small clusters
                
                hull=convhull(double_line_data_filt[:,x_coord_idx:y_coord_idx+1])
                # When input points are 2-dimensional, this is the perimeter of the convex hull:
                hull_perimeter=hull.area
                # When input points are 2-dimensional, volume is the area of the convex hull:
                hull_area=hull.volume
                
                
                # # filter out small objects
                # if hull_area<min_obj_area:
                #     continue
            
                
                # filter localizations according to the localization precision and sigma value
                std_x=double_line_data_filt[:,3]
                std_y=double_line_data_filt[:,4]
                sig_x=double_line_data_filt[:,6]
                sig_y=double_line_data_filt[:,7]

                # filter according to loc. precision
                std_x_filt_locs=np.logical_and(std_x>min_std_xy, std_x<max_std_xy)
                std_y_filt_locs=np.logical_and(std_y>min_std_xy, std_y<max_std_xy)
                std_filt_locs=np.logical_and(std_x_filt_locs,std_y_filt_locs)
                # filter according to sigma
                sig_x_filt_locs=np.logical_and(sig_x>min_sigma_xy, sig_x<max_sigma_xy)
                sig_y_filt_locs=np.logical_and(sig_y>min_sigma_xy, sig_y<max_sigma_xy)
                sig_filt_locs=np.logical_and(sig_x_filt_locs,sig_y_filt_locs)
                
                filt_locs2=np.logical_and(std_filt_locs,sig_filt_locs)
                double_line_data_filt=double_line_data_filt[filt_locs2,:]
            

                # memory release
                del feature_vector
                
                
                
                
                
                
                
                
                # figure
                fig,axs=pyplot.subplots(2,2)
                axs[0, 0].scatter(selected_meas_data[:,y_coord_idx],-selected_meas_data[:,x_coord_idx],s=0.1,color="k")
                axs[0, 0].axis('equal')
                axs[0, 0].set_title('all loc.', fontsize=10)
                axs[0, 0].axes.xaxis.set_visible(False)
                axs[0, 0].axes.yaxis.set_visible(False)

                axs[0, 1].scatter(double_line_data[:,y_coord_idx],-double_line_data[:,x_coord_idx],s=0.1,color="violet")
                axs[0, 1].scatter(double_line_data_filt[:,y_coord_idx],-double_line_data_filt[:,x_coord_idx],s=0.1,color="m")
                axs[0, 1].axis('equal')
                axs[0, 1].set_title('object loc.', fontsize=10)
                axs[0, 1].axes.xaxis.set_visible(False)
                axs[0, 1].axes.yaxis.set_visible(False)

                axs[1, 0].scatter(selected_meas_data[~is_object_coord,y_coord_idx],-selected_meas_data[~is_object_coord,x_coord_idx],s=0.1,color="c")
                #axs[1, 0].scatter(double_line_data[~is_filt_object_coord,y_coord_idx],-double_line_data[~is_filt_object_coord,x_coord_idx],s=0.1,color="darkcyan")
                axs[1, 0].axis('equal')
                axs[1, 0].set_title('noise loc.', fontsize=10)
                axs[1, 0].axes.xaxis.set_visible(False)
                axs[1, 0].axes.yaxis.set_visible(False)

                axs[1, 1].scatter(selected_y,-selected_x,s=0.1,color="c")
                axs[1, 1].scatter(double_line_data_filt[:,y_coord_idx],-double_line_data_filt[:,x_coord_idx],s=0.1,color="m")
                axs[1, 1].axis('equal')
                axs[1, 1].set_title('labeled loc.', fontsize=10)
                axs[1, 1].axes.xaxis.set_visible(False)
                axs[1, 1].axes.yaxis.set_visible(False)
                
                
                
                
                
                "save results"
                #double_line_data_filt[:,x_coord_idx]=double_line_data_filt[:,x_coord_idx]/EMCCD_pixel_size
                #double_line_data_filt[:,y_coord_idx]=double_line_data_filt[:,y_coord_idx]/EMCCD_pixel_size
                # ROI center
                centerCol=round(min(double_line_data_filt[:,x_coord_idx])/EMCCD_pixel_size+((max(double_line_data_filt[:,x_coord_idx])-min(double_line_data_filt[:,x_coord_idx]))/2)/EMCCD_pixel_size)
                centerRow=round(min(double_line_data_filt[:,y_coord_idx])/EMCCD_pixel_size+((max(double_line_data_filt[:,y_coord_idx])-min(double_line_data_filt[:,y_coord_idx]))/2)/EMCCD_pixel_size)
                roi_coords_name=meas_id+'_perim'+str(round(hull_perimeter))+'_area'+str(round(hull_area/1000))+'k_'+'_export_X'+str(centerCol)+'_Y'+str(centerRow)
                roi_coords_plot_name=roi_coords_name+'_scatter.png'
                pyplot.savefig(os.path.join(path2saveRes, roi_coords_plot_name), dpi=200)
                pyplot.close()
                
                # save results (object localization coordinates) into csv files for further (IFM) analysis
                double_line_data_filt=double_line_data_filt[double_line_data_filt[:,0].argsort()]
                hdr_str="frame_idx,x_coord,y_coord,x_std,y_std,I,sig_x,sig_y,Sum_signal"
                roi_coords_csv_name=roi_coords_name+'.csv'
                np.savetxt(os.path.join(path2saveRes, roi_coords_csv_name), double_line_data_filt, delimiter=",", header=hdr_str, comments='')
                
        time_end = time.time()
        print('_______________________________________________')
        print(meas_id+' evaluation: '+str(round(((time_end-time_start)/60)*10)/10)+' min')
        print('_______________________________________________')
        
        

       







